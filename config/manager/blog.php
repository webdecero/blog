<?php

// Noticias
$pages['index'] = [
    'isSeo' => TRUE,
    'menuTitle' => 'Blog',
    'sectionName' => 'Blog',
    'sectionAction' => 'Editar',
    'sections' =>
    [

        'imagen' => [
            'element' => 'imageResize',
            'label' => '',
            'required' => true,
            'w' => '700',
            'h' => '700',
            'resize' => 'widen',
            'path' => 'img/blog',
            'ratio' => '',
            'format' => 'png',
        ],
        'video' => [
            'element' => 'inputText',
            'label' => 'Video Vimeo ',
            'required' => false,
        ],
        'titulo' => [
            'element' => 'inputText',
            'label' => 'Título ',
            'required' => true,
            'maxlength' => 60
        ],
        'descripcionCorta' => [
            'element' => 'inputText',
            'label' => 'Descripcion Corta ',
            'required' => true,
            'maxlength' => 155
        ],
        'tituloLargo' => [
            'element' => 'inputText',
            'label' => 'Título largo',
            'required' => true,
        ],
        'contenido' => [
            'element' => 'inputTextarea',
            'label' => 'Contenido',
            'required' => true,
        ],
        'galeria' =>[
            'element' => 'inputDynamicSelect',
            'label' => 'Galeria',
            'required' => false,
            'options' => [
                 
             ],
        ],
        'category'=>[
            'element' => 'inputText',
            'label' => 'Categoria',    
            'dataServiceCategories' => 'manager.blog.categories', //active autocomplete for categories
            'id' => 'categoria',
            'helper' => "Categorias separadas por ','"        
        ],
        'tags' => [
            'element' => 'inputText',
            'label' => 'Palabras Clave',
            'required' => true,
            'id'=> 'tags',
            'dataServiceTags' => 'manager.gallery.tags',
            'helper' => "Palabras clave separadas por ','"
        ],
        'datePublication'=>[
            'element' => 'inputText',
            'label' => 'Fecha de Publicación',
            'helper'=>'Utilizar 24 Horas DD-MM-YYYY HH:MM'            
        ],
        'status' => [
            'element' => 'inputSelect',
            'label' => 'Estatus',
            'required' => true,
             'options' => [
                 'true' =>'Si',
                 'false' =>'No'
             ],
             'helper'=>'Si el estatus es no visible el artículo no estara visible dentro del sitio'
        ],
        'locale' => [
            'element' => 'inputSelect',
            'label' => 'Idioma',
            'required' => true,
        ],

  ],
];


return $pages;