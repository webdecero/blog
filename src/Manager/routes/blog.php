<?php

/*
  |--------------------------------------------------------------------------
  | Web Blog Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//Blog

Route::post('blog/dataTable/{id?}/{excel?}', array('as' => 'manager.blog.dataTable', 'uses' => 'BlogDataTableController@dataTable'));
Route::get('blog/{id}/delete', ['as' => 'manager.blog.destroy', 'uses' => 'BlogController@destroy']);
Route::get('blog/tags', ['as' => 'manger.blog.tags', 'uses' => 'BlogController@getTags']);
Route::get('blog/categories', ['as' => 'manager.blog.categories', 'uses' => 'BlogController@getCategories']);
Route::resource('blog', 'BlogController', [
    'except' => [
        'show', 'destroy'
    ],
    'names' => [
        'index' => 'manager.blog.index',
        'create' => 'manager.blog.create',
        'store' => 'manager.blog.store',
        'show' => 'manager.blog.show',
        'edit' => 'manager.blog.edit',
        'update' => 'manager.blog.update',
        //'destroy' => 'manager.blog.destroy',
    ]
]);