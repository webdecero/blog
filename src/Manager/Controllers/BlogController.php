<?php

namespace Webdecero\Blog\Manager\Controllers;

//Providers
use Validator;
use Auth;
//Models
use Webdecero\Blog\Manager\Models\Blog;
use Webdecero\Gallery\Manager\Models\Gallery;
//Helpers and Class
use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
use Webdecero\Base\Manager\Controllers\ManagerController;
use Webdecero\Base\Manager\Traits\AutocompleteTags;
use Webdecero\Base\Manager\Traits\DynamicInputs;
use Webdecero\Base\Manager\Facades\Utilities;
use Webdecero\Base\Pages\Traits\Seo\SeoParams;

class BlogController extends ManagerController {

    use AutocompleteTags;
    use DynamicInputs;
    use SeoParams;

    public function __construct() {

        parent::__construct();
        $this->autocompleteResource = new Blog;

        $this->configPages = config('manager.blog');
        foreach (\LaravelLocalization::getSupportedLocales() as $key => $value) {
            $this->arrayLocaleNative[$key] = $value['native'];
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $this->data['user'] = Auth::user();
        // SE agrega un filtro para las categorias
        $this->data['categories'] = Blog::getCategories();

        //dd($this->date['categories']);
        return view('baseViews::blogPanel', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

		//$this->autocompleteResource = new Gallery;

        $galerias = Gallery::getCategories();
        //dd($galerias);
        $this->data['user'] = Auth::user();
        $this->data['dataPage'] = [];
        $type = request('type', 'index');
        $configPage = $this->configPages[$type];
        $configPage['sections']['locale']['options'] = $this->arrayLocaleNative;
        // Busco las categorias
        $configPage['sections']['galeria']['options'] = $galerias;
        //dd($galerias);
        $processData = $this->processPageUI($configPage, $this->data);
        return view('baseViews::blogAddForm', $processData);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $input = $request->all();
        $rules = array(
            'titulo' => array('required', 'unique:blog,titulo'),
            'descripcionCorta' => array('required'),
            'tituloLargo' => array('required', 'unique:blog,tituloLargo'),
            'contenido' => array('required'),
            //'categoria' => array('required'),
            'tags' => array('required'),
            //'publicacion' => array('required')
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return redirect()->route("manager.blog.create")->withErrors($validator)->with([
                        'error' => trans('baseLang::mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {
            $blog = new Blog;
            $this->_storeBlog($request, $blog);
            return redirect()->route("manager.blog.index")->with([
                        'mensaje' => trans('baseLang::mensajes.registro.exito'),
            ]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $blog = Blog::findOrFail($id);
        $isValid = Utilities::idMongoValid($id);
        if (!$isValid || !isset($blog->id)) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }
        $this->data['user'] = Auth::user();
        $this->data['blog'] = $blog;
        $this->data['dataPage'] = $blog->toArray();
        $this->setSeoParams($this->data['blog']);

        //dd($this->data);
        $galerias = Gallery::getCategories();
        $type = request('type', 'index');
        $configPage = $this->configPages[$type];
        $configPage['sections']['locale']['options'] = $this->arrayLocaleNative;
        $configPage['sections']['galeria']['options'] = $galerias;
        $processData = $this->processPageUI($configPage, $this->data);
        //dd($processData);
        return view('baseViews::blogEditForm', $processData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $input = $request->all();
        $rules = array(
            'imagen' => array('required'),
            'titulo' => array('required'),
            'descripcionCorta' => array('required'),
            'tituloLargo' => array('required'),
            'contenido' => array('required'),
            //'category' => array('required'),
            'tags' => array('required'),
            //'datePublication' => array('required')
        );
        $input['id'] = $id;
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->with([
                        'error' => trans('baseLang::mensajes.registro.incompleto'),
                    ])->withInput($request->except('password'));
        } else {
            $isValid = Utilities::idMongoValid($input['id']);
            $blog = Blog::find($input['id']);
            if (!$isValid || !isset($blog->id)) {
                return redirect()->route('manager.blog.index')->with([
                            'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
                ]);
            }
            $this->_storeBlog($request, $blog);
            return redirect()->route('manager.blog.index')->with([
                        'mensaje' => trans('baseLang::mensajes.registro.exito'),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $isValid = Utilities::idMongoValid($id);
        $blog = Blog::findOrFail($id);
        if (!$isValid || !isset($blog->id)) {
            return back()->with([
                        'error' => trans('baseLang::mensajes.operacion.noEncotrado'),
            ]);
        }
        $blog->delete();
        return redirect()->route('manager.blog.index')->with([
                    'mensaje' => trans('baseLang::mensajes.operacion.correcta'),
        ]);
    }

    private function _storeBlog(Request $request, $blog) {
        $input = $request->all();
        $blog->fill($input);
        $blog->slug = Utilities::slug($input['titulo']);
        $type = request('type', 'index');
        $sections = $this->configPages[$type]['sections'];
        $blog = $this->dynamicFillSave($blog, $sections, $input);
        $blog->save();
        return $blog;

    }

}
