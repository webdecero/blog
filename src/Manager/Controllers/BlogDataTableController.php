<?php

namespace Webdecero\Blog\Manager\Controllers;

//Providers
//Models
//Helpers and Class
use Webdecero\Base\Manager\Controllers\DataTableController;
use Webdecero\Base\Manager\Facades\Utilities;


class BlogDataTableController extends DataTableController {

    protected $collectionName = 'blog';
    
    protected $searchable = [
        'query' => [
            '_id' => 'contains',
            'titulo' => 'contains',
            'descripcionCorta' => 'contains',
            'category' => 'contains',
            'tags' => 'contains',
        ],
        'status' => 'equal',
    ];
    
    
    protected $fieldsDetails = [
        '_id' => 'ID',
        'titulo' => 'Título',
        'tituloLargo' => 'Título Largo',
        'descripcionCorta' => 'Descripción',
        'slug' => 'Slug',
        'tituloLargo' => 'Titulo Largo',
        'contenido' => 'Contenido',
        'category' => 'Categoría',
        'tags' => 'Tags',
        'status' => 'Publicado',
        'imagen' => 'Imagen',
        'video' => 'video',
        'locale' => 'Idioma',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    protected $fieldsDetailsExcel = [
        '_id' => 'ID',
        'titulo' => 'Título',
        'tituloLargo' => 'Título Largo',
        'descripcionCorta' => 'Descripción',
        'slug' => 'Slug',
        'tituloLargo' => 'Titulo Largo',
        'contenido' => 'Contenido',
        'category' => 'Categoría',
        'tags' => 'Tags',
        'status' => 'Publicado',
        'imagen' => 'Imagen',
        'video' => 'Video',
        'locale' => 'Idioma',
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {
        $record = parent::formatRecord($field, $item);
        if ($field == 'tags' && isset($item[$field])) {
            $record = \Utilities::implodeTags((array)$item[$field],',');
        }else    if ($field == 'category' && isset($item[$field])) {
            $record = \Utilities::implodeTags((array)$item[$field],',');
        } else if ($field == 'imagen' && isset($item[$field])) {
            $record = "<img class='thumb' src=" . $item[$field] . " >";
        }else if($field =='status'){
            if($item[$field]){
                $record = "Publicado";
            }else{
                $record = "No Publicado";
            }
        } else if ($field == 'url_details') {
            $record = route('manager.blog.dataTable', ['id' => $item['_id']]);
        } else if ($field == 'opciones') {
            $record = "<a href=" . route('manager.blog.edit', ['id' => $item['_id']]) . "  class='text-center center-block' data-toggle='tooltip' title='Editar' > <i class='fa fa-pencil-square-o'></i> </a>";
            $record .= "<a href=" . route('manager.blog.destroy', ['id' => $item['_id']]) . "  class='text-center center-block borrar'  data-toggle='tooltip' title='Eliminar'  > "
                    . "<i class='fa fa-trash-o'></i> "
                    . "</a>";
        }
        return $record;
    }

  
}
