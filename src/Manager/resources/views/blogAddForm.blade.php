@extends($layout)


@section('sectionAction', 'Agregar')
@section('sectionName', $sectionName)
@section('formAction', route('manager.blog.store'))

@section('inputs')

{!! $htmlInputs !!}

@endsection