@extends($layout)


@section('sectionAction', 'Editar')
@section('sectionName', $sectionName)
@section('formAction', route('manager.blog.update',[$blog->id]))

@section('inputs')
{{ method_field('PUT') }}

{!! $htmlInputs !!}

@endsection


