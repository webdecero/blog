<?php

namespace Webdecero\Blog\Manager\Models;
use Webdecero\Base\Manager\Facades\Utilities;


use Webdecero\Base\Manager\Models\Base as Base;

class Blog extends Base {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'blog';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
//    protected $fillable = ['titulo', 'tituloLargo', 'descripcionCorta', 'contenido', 'imagen', 'video', 'slug', 'tags', 'categoria', 'fechaPublicacion', 'publicacion'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
     public function __construct() {

     }


    // public function getTagsAttribute($array)
    // {
    //     //dd($array);
    //     return Utilities::implodeTags($array,',');
    // }


    public function getCategoryAttribute($array)
    {
        //dd($array);
        return Utilities::implodeTags($array,',');
    }

    public function getDatePublicationAttribute($date)
    {
        //dd($date);
        return Utilities::isoDateToString($date);
    }

 
//    public function countComentarios() {
//
//
//
//        $comentarios = $this->comentarios;
//
//
//        if ($comentarios == NULL) {
//
//            return 0;
//        } else {
//            return count($comentarios);
//        }
//    }


}
