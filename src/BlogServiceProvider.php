<?php

namespace Webdecero\Blog;

use Illuminate\Support\ServiceProvider;
use RuntimeException;
use Illuminate\Support\Facades\Route;

//use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;


//use Illuminate\Foundation\ProviderRepository;
class BlogServiceProvider extends ServiceProvider {

    
    
    private $configBlog = __DIR__ . '/../config/manager/blog.php';
    
    private $folderViews = __DIR__ . '/Manager/resources/views';
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {

       
//        Publishes Configuración Base
//        php artisan vendor:publish --provider="Webdecero\Blog\BlogServiceProvider" --tag=config

        $this->publishes([
            $this->configBlog => config_path('manager/blog.php'),
            
                ], 'config');

//        Registra Views
        $this->loadViewsFrom($this->folderViews, 'baseViews');

//ROUTE Blog 
        $config = $this->app['config']->get('manager.base.manager.route', []);

        if (empty($config)) {
            throw new RuntimeException('No se enecontro la configuración Base para ruta');
        }

        $config['namespace'] = 'Webdecero\Blog\Manager\Controllers';
        
        $originalMiddleware =$config['middleware'];

 
        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'editRoute']);

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/blog.php');
        });
        

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {

        $this->mergeConfigFrom($this->configBlog, 'manager.blog');
//        $this->app->make('Webdecero\Slide\Manager\Controllers\SlideController');
    }

}
